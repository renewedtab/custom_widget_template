# Custom Widget Template

A template for creating your own Renewed Tab widgets using TypeScript and React

See [Creating Your Own Custom Widgets for Renewed Tab](https://renewedtab.com/blog/2022/07/24/custom-widgets/).

License: MIT or CC 0 or Public Domain

Note: the Renewed Tab .css stylesheet is under
[Renewed Tab's license](https://gitlab.com/renewedtab/renewedtab/-/blob/dev/LICENSE.md).

## Usage

* Install deps: `npm install`
* Build: `npm run build`
* Run test server: `npm start`

After running `npm run build`, you can find the output files in `dist/`.
These files can then be hosted on a server (such as GitLab/GitHub Pages) and
distributed to users.
