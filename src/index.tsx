import React from 'react'
import { createRoot } from 'react-dom/client';



import "./scss/main.scss";


function App() {
    return (
        <>
		<h2>Hello World!</h2>
		<p>
			This is a custom widget shown using an IFrame!
		</p>
		<button className="btn btn-primary">Button</button>
		</>);
}


const root = createRoot(document.getElementById("app")!);
root.render(<App />);
